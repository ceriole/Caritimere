package cereal.cmere;

import java.awt.Image;
import java.io.File;
import java.util.ArrayList;

public class GameInfo {

	public static final String NAME = "Caritimere";
	public static final int WIDTH = 800, HEIGHT = WIDTH / 4 * 3;

	public static final String HANDLE = ">]", CARAT = "[";

	public static final int[] MARGIN = new int[] { 10, 5 }; // Top, left
	public static final int TEXT_WIDTH = WIDTH - MARGIN[1] * 2;

	public static final String VERSION = "0.1.081dev";
	public static final String WHATSNEW = "+ Command suggestion\n" + "~ Re-Wrote command system\n" + "+ Added minimap\n" + "~ Text is smol now\n" + "~ Fixed minimap transitions";

	public static final long SECONDS_PER_MINUTE = 60;
	public static final long MINUTES_PER_HOUR = 60;
	public static final long HOURS_PER_DAY = 24;
	public static final long DAYS_PER_YEAR = 120;

	public static final long YEAR_START = 5374;
	public static final long DAY_START = 1;
	public static final long HOUR_START = 8;

	public static final ArrayList<Image> WINDOW_ICONS = new ArrayList<Image>();

	public static final String APPDATA_FOLDER_NAME = "caritimere";

	public static final String SAVE_FOLDER_PATH = GameInfo.getAppdataFolderPath() + File.separator + "saves";

	public static void init() {
		WINDOW_ICONS.add(Utils.loadImage("/icons/icon64.png"));
		WINDOW_ICONS.add(Utils.loadImage("/icons/icon32.png"));
		WINDOW_ICONS.add(Utils.loadImage("/icons/icon16.png"));
	}

	public static String getAppdataFolderPath() {
		String OS = (System.getProperty("os.name")).toUpperCase();
		if (OS.contains("WIN")) {
			return System.getenv("AppData") + File.separator + APPDATA_FOLDER_NAME;
		} else {
			return System.getProperty("user.home") + File.separator + "Library" + File.separator + "Application Support" + File.separator + APPDATA_FOLDER_NAME;
		}
	}

}
