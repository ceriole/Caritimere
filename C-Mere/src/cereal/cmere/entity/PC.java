package cereal.cmere.entity;

import com.google.common.collect.ImmutableList;

import cereal.cmere.Game;
import cereal.cmere.game.IStatistical;
import cereal.cmere.game.command.GameCommand;

public class PC extends NPC implements IStatistical {

	public PC(String name) {
		super(name);
	}

	@Override
	public String getDescription() {
		return "It's you, " + getName() + ".";
	}

	@Override
	public boolean isHidden() {
		return false;
	}

	@Override
	public boolean isInteractable() {
		return true;
	}

	@Override
	public ImmutableList<String> getAliases() {
		return ImmutableList.of("me", "self", "pc");
	}

	public void setName(String playerName) {
		this.name = playerName;
	}

	@Override
	public void onInteracted(GameCommand<? extends GameCommand<?>> cmd) {
		super.onInteracted(cmd);
	}

	@Override
	public void setSagacity(int sag) {
		super.setSagacity(sag);
		Game.instance().onPlayerStatChange();
	}

	@Override
	public void setTenacity(int ten) {
		super.setTenacity(ten);
		Game.instance().onPlayerStatChange();
	}

	@Override
	public void setVivacity(int viv) {
		super.setVivacity(viv);
		Game.instance().onPlayerStatChange();
	}

	@Override
	public int setHealth(int health) {
		Game.instance().onPlayerStatChange();
		return super.setHealth(health);
	}

	@Override
	public int addHealth(int amt) {
		Game.instance().onPlayerStatChange();
		return super.addHealth(amt);
	}

	@Override
	public int setLust(int lust) {
		Game.instance().onPlayerStatChange();
		return super.setLust(lust);
	}

	@Override
	public int addLust(int amt) {
		Game.instance().onPlayerStatChange();
		return super.addLust(amt);
	}

	@Override
	public int setFullness(int fullness) {
		Game.instance().onPlayerStatChange();
		return super.setFullness(fullness);
	}

	@Override
	public int addFullness(int amt) {
		Game.instance().onPlayerStatChange();
		return super.addFullness(amt);
	}

	@Override
	public String getSerialName() {
		return "pc";
	}

}
