package cereal.cmere.entity;

import com.google.common.collect.ImmutableList;

import cereal.cmere.game.IStatistical;
import cereal.cmere.game.command.GameCommand;
import net.keabotstudios.superserial.containers.SSField;
import net.keabotstudios.superserial.containers.SSObject;

public class NPC extends Entity implements IStatistical {

	protected int level, maxLevel = 50;
	protected int xp;
	protected float maxXpIncPerLevel = 1.025f;
	protected int sagacity = 3;
	protected int tenacity = 3;
	protected int vivacity = 3;

	protected int health, lust, fullness;

	public NPC(String name) {
		super(name);
		health = getMaxHealth();
	}

	@Override
	public boolean isHidden() {
		return false;
	}

	@Override
	public boolean isInteractable() {
		return false;
	}

	@Override
	public ImmutableList<String> getAliases() {
		return null;
	}

	@Override
	public void onInteracted(GameCommand<? extends GameCommand<?>> cmd) {

	}

	@Override
	public int getLevel() {
		return level;
	}

	@Override
	public int getMaxLevel() {
		return maxLevel;
	}

	@Override
	public int getXp() {
		return xp;
	}

	@Override
	public int getMaxXp() {
		return 500 + 200 * (getLevel() - 1);
	}

	@Override
	public void setXp(int xp) {
		if (xp < 0)
			return;
		this.xp = xp;
		while (this.xp >= getMaxXp()) {
			this.xp -= getMaxXp();
			this.level++;
		}
	}

	@Override
	public void addXp(int amt) {
		this.xp += amt;
		while (this.xp >= getMaxXp()) {
			this.xp -= getMaxXp();
			this.level++;
		}
	}

	@Override
	public int getSagacity() {
		return sagacity;
	}

	@Override
	public void setSagacity(int sag) {
		this.sagacity = sag;
	}

	@Override
	public int getTenacity() {
		return tenacity;
	}

	@Override
	public void setTenacity(int ten) {
		this.tenacity = ten;
	}

	@Override
	public int getVivacity() {
		return vivacity;
	}

	@Override
	public void setVivacity(int viv) {
		this.vivacity = viv;
	}

	@Override
	public boolean canTarget() {
		return true;
	}

	@Override
	public int getMaxHealth() {
		return 3 + getTenacity() * 2;
	}

	@Override
	public int getHealth() {
		return health;
	}

	@Override
	public int setHealth(int health) {
		this.health = Math.min(Math.max(health, 0), getMaxHealth());
		return this.health;
	}

	@Override
	public int addHealth(int amt) {
		this.health = Math.min(Math.max(this.health + amt, 0), getMaxHealth());
		return this.health;
	}

	@Override
	public int getMaxLust() {
		return Math.max(5, 5 + (Math.max(0, getVivacity() - 5) * -15) + (getSagacity() * 3));
	}

	@Override
	public int getLust() {
		return lust;
	}

	@Override
	public int setLust(int lust) {
		this.lust = Math.min(Math.max(lust, 0), getMaxLust());
		return this.lust;
	}

	@Override
	public int addLust(int amt) {
		this.lust = Math.min(Math.max(this.lust + amt, 0), getMaxLust());
		return this.lust;
	}

	@Override
	public int getMaxFullness() {
		return 5 + getTenacity() * 2;
	}

	@Override
	public int getFullness() {
		return fullness;
	}

	@Override
	public int setFullness(int fullness) {
		this.fullness = Math.min(Math.max(this.fullness, 0), getMaxFullness());
		return this.fullness;
	}

	@Override
	public int addFullness(int amt) {
		this.fullness = Math.min(Math.max(this.fullness + amt, 0), getMaxFullness());
		return this.fullness;
	}

	@Override
	public long getGetMoveLength() {
		return Math.max(5, 60 - (4 * Math.max(1, this.tenacity - 5)));
	}

	@Override
	public void serializeEntity(SSObject obj) {
		obj.addField(SSField.Integer("lvl", getLevel()));
		obj.addField(SSField.Integer("exp", getXp()));

		obj.addField(SSField.Integer("sag", getSagacity()));
		obj.addField(SSField.Integer("ten", getTenacity()));
		obj.addField(SSField.Integer("viv", getVivacity()));

		obj.addField(SSField.Integer("hp", getSagacity()));
		obj.addField(SSField.Integer("lus", getTenacity()));
		obj.addField(SSField.Integer("ful", getVivacity()));
	}

	@Override
	public void deserializeEntity(SSObject data) {
		this.level = data.getField("lvl").getInteger();
		this.xp = data.getField("exp").getInteger();

		this.sagacity = data.getField("sag").getInteger();
		this.tenacity = data.getField("ten").getInteger();
		this.vivacity = data.getField("viv").getInteger();

		this.health = data.getField("hp").getInteger();
		this.lust = data.getField("lus").getInteger();
		this.fullness = data.getField("ful").getInteger();
	}

}
