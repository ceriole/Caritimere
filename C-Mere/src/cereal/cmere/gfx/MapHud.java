package cereal.cmere.gfx;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;

import cereal.cmere.Game;
import cereal.cmere.GameInfo;
import cereal.cmere.map.Direction;
import cereal.cmere.map.MapPos;
import cereal.cmere.map.area.Area;

public class MapHud {

	private BufferedImage image;
	private int width = 150, height = 150, x = GameInfo.WIDTH - width - 10, y = 10;
	private int ox, oy, cy, cx, fx, fy;
	private long timer, length;
	private MapPos lastPos;
	private boolean moving;

	public MapHud() {
		image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		lastPos = Game.instance().getPlayer().getPos();
	}

	public void draw(Graphics2D gs) {
		Graphics2D g = (Graphics2D) image.getGraphics();
		g.setBackground(new Color(0, 0, 0, 0));
		g.clearRect(0, 0, width, height);
		Shape bgCircle = new Ellipse2D.Double(0, 0, width, height);
		g.setClip(bgCircle);
		g.setColor(Color.BLACK);
		g.fill(bgCircle);
		g.translate(width / 2, height / 2);
		for (Area a : Game.instance().getCurrentMap().getAreas()) {
			int x = cx + a.getMiniMapX();
			int y = cy - a.getMiniMapY();
			g.translate(x, y);
			g.setColor(Color.WHITE);
			for (Direction d : a.getExits()) {
				Area exitTarg = Game.instance().getCurrentMap().getAreaAt(d.from(a));
				if (exitTarg == null)
					continue;
				if (!exitTarg.isSideOpen(d.opposite()))
					continue;
				g.drawLine(0, 0, -(a.getMiniMapX() - exitTarg.getMiniMapX()), a.getMiniMapY() - exitTarg.getMiniMapY());
			}
			g.translate(-x, -y);
		}
		for (Area a : Game.instance().getCurrentMap().getAreas()) {
			int x = cx + a.getMiniMapX();
			int y = cy - a.getMiniMapY();
			g.translate(x, y);
			a.drawOnMiniMap(g);
			g.translate(-x, -y);
		}
		g.setColor(Color.ORANGE);
		g.setStroke(new BasicStroke(3));
		int playerRad = 3 + (int) Math.round(2 * (0.5f + 0.5f * Math.sin(Math.PI * 2 * (Game.instance().getTimer() % 400) / 400)));
		g.fill(new Ellipse2D.Double(-playerRad, -playerRad, playerRad * 2, playerRad * 2));
		g.translate(-width / 2, -height / 2);
		g.dispose();
		Composite old = gs.getComposite();
		gs.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, 0.9f));
		gs.drawImage(image, x, y, width, height, null);
		gs.setComposite(old);
	}

	public void update() {

		if (Game.instance().getPlayer() == null || Game.instance().getPlayer().getCurrentArea() == null)
			return;
		if (lastPos != Game.instance().getPlayer().getPos() && !moving) {
			moveToNewPos(30);
		}

		if (moving) {
			if (Game.instance().getTimer() - timer >= length) {
				moving = false;
				cx = fx;
				cy = fy;
				ox = fx = 0;
				oy = fy = 0;
				lastPos = Game.instance().getPlayer().getPos();
				return;
			}
			float transition = (float) ((Game.instance().getTimer() - timer) / (double) length);
			fx = -Game.instance().getPlayer().getCurrentArea().getMiniMapX();
			fy = Game.instance().getPlayer().getCurrentArea().getMiniMapY();
			ox = -Game.instance().getCurrentMap().getAreaAt(lastPos).getMiniMapX();
			oy = Game.instance().getCurrentMap().getAreaAt(lastPos).getMiniMapY();
			cx = Math.round(ox * (1f - transition) + fx * (transition));
			cy = Math.round(oy * (1f - transition) + fy * (transition));
		}

		lastPos = Game.instance().getPlayer().getPos();
	}

	private void moveToNewPos(long length) {
		timer = Game.instance().getTimer();
		moving = true;
		this.length = length;
	}

}
