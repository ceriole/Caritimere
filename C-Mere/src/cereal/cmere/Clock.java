package cereal.cmere;

import cereal.cmere.io.ISSerializable;
import net.keabotstudios.superserial.containers.SSField;
import net.keabotstudios.superserial.containers.SSObject;

public class Clock implements ISSerializable {

	private long seconds;
	private long secMin, minHr, hrDay, dayYr;

	public Clock(long seconds, long secMin, long minHr, long hrDay, long dayYr) {
		this.seconds = 0;

		this.secMin = secMin;
		this.minHr = minHr;
		this.hrDay = hrDay;
		this.dayYr = dayYr;
	}

	public Clock(long seconds) {
		this(seconds, GameInfo.SECONDS_PER_MINUTE, GameInfo.MINUTES_PER_HOUR, GameInfo.HOURS_PER_DAY, GameInfo.DAYS_PER_YEAR);
	}

	public Clock() {
		this(0);
	}

	public long tick() {
		return ++seconds;
	}

	public Clock jump(long seconds) {
		this.seconds += seconds;
		return this;
	}

	public Clock jumpMinutes(long minutes) {
		return jump(minutes * secMin);
	}

	public Clock jumpHours(long hours) {
		return jumpMinutes(hours * minHr);
	}

	public Clock jumpDays(long days) {
		return jumpHours(days * hrDay);
	}

	public Clock jumpYears(long years) {
		return jumpDays(years * dayYr);
	}

	public Clock setTime(long seconds) {
		this.seconds = seconds;
		return this;
	}

	public Clock setTimeMinutes(long minutes) {
		return setTime(minutes * secMin);
	}

	public Clock setTimeHours(long hours) {
		return setTimeMinutes(hours * minHr);
	}

	public Clock setTimeDays(long days) {
		return setTimeHours(days * hrDay);
	}

	public Clock setTimeYears(long years) {
		return setTimeDays(years * dayYr);
	}

	public long getTotalTimeSeconds() {
		return seconds;
	}

	public long getTotalTimeMinutes() {
		return getTotalTimeSeconds() / secMin;
	}

	public long getTotalTimeHours() {
		return getTotalTimeMinutes() / minHr;
	}

	public long getTotalTimeDays() {
		return getTotalTimeHours() / hrDay;
	}

	public long getTotalTimeYears() {
		return getTotalTimeDays() / dayYr;
	}

	public long getTimeSeconds() {
		return getTotalTimeSeconds() % secMin;
	}

	public long getTimeMinutes() {
		return (getTotalTimeSeconds() / secMin) % minHr;
	}

	public long getTimeHours() {
		return (getTotalTimeMinutes() / minHr) % hrDay;
	}

	public long getTimeDays() {
		return (getTotalTimeHours() / hrDay) % dayYr;
	}

	public long getTimeYears() {
		return getTotalTimeYears();
	}

	public String toString() {
		return String.format("Day %02d, Year %04d; %02d:%02d:%02d", getTimeDays(), getTimeYears(), getTimeHours(), getTimeMinutes(), getTimeSeconds());
	}

	public String toShortString() {
		return String.format("Hour %02d, Day %02d, %04d", getTimeHours(), getTimeDays(), getTimeYears());
	}

	@Override
	public SSObject serialize() {
		SSObject object = new SSObject(getSerialName());
		object.addField(SSField.Long("seconds", seconds));
		object.addField(SSField.Long("secMin", secMin));
		object.addField(SSField.Long("minHr", minHr));
		object.addField(SSField.Long("hrDay", hrDay));
		object.addField(SSField.Long("dayYr", dayYr));
		return object;
	}

	@Override
	public void deserialize(SSObject data) {
		seconds = data.getField("seconds").getLong();
		secMin = data.getField("secMin").getLong();
		minHr = data.getField("minHr").getLong();
		hrDay = data.getField("hrDay").getLong();
		dayYr = data.getField("dayTr").getLong();
	}

	@Override
	public String getSerialName() {
		return "clock";
	}

}
