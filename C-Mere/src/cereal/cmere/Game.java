package cereal.cmere;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferStrategy;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import cereal.cmere.entity.Entity;
import cereal.cmere.entity.PC;
import cereal.cmere.game.command.CommandInputBar;
import cereal.cmere.game.command.CommandParser;
import cereal.cmere.game.command.GameCommand;
import cereal.cmere.game.command.GameParseException;
import cereal.cmere.gfx.MapHud;
import cereal.cmere.gfx.StatHud;
import cereal.cmere.gfx.TextRenderer;
import cereal.cmere.map.LabMap;
import cereal.cmere.map.Map;
import cereal.cmere.map.MapPos;
import net.keabotstudios.superserial.SSException;
import net.keabotstudios.superserial.containers.SSDatabase;
import net.keabotstudios.superserial.containers.SSField;
import net.keabotstudios.superserial.containers.SSObject;
import net.keabotstudios.superserial.containers.SSString;

public class Game extends Canvas implements Runnable, KeyListener, FocusListener, MouseWheelListener {
	private static final long serialVersionUID = 1L;

	private boolean autoScroll = true;

	private static Font GAME_FONT;
	static {
		try {
			GAME_FONT = Font.createFont(Font.TRUETYPE_FONT, Game.class.getResourceAsStream("/font/TerminalVector.ttf")).deriveFont(Font.PLAIN, 12f);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static Game INSTANCE;

	private JFrame window;
	private Thread thread;
	private boolean running;
	private int turnQueue;

	private Map currentMap;
	private PC player;

	private String screenText = "";
	private CommandInputBar inputBar;
	private StatHud playerHud;
	private MapHud mapHud;

	private long timer;

	private Clock clock;

	private boolean hasFocus = false;

	private boolean awaitingTextInput;
	private String textInput;

	private int screenOffset = 0;

	public Game() {
		INSTANCE = this;
		window = new JFrame(GameInfo.NAME);
		Dimension size = new Dimension(GameInfo.WIDTH, GameInfo.HEIGHT);
		this.setMinimumSize(size);
		this.setPreferredSize(size);
		this.setMaximumSize(size);
		this.setSize(size);
		this.setFocusable(true);
		this.requestFocusInWindow();
		this.addKeyListener(this);
		this.addFocusListener(this);
		this.addMouseWheelListener(this);

		window.setResizable(false);
		window.add(this);
		window.pack();
		window.setLocationRelativeTo(null);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		inputBar = new CommandInputBar();
		window.setVisible(true);

		currentMap = new LabMap();
		player = new PC("unnamed");
		player.setPos(new MapPos(0, 0, 0));

		playerHud = new StatHud(player);

		mapHud = new MapHud();

		clock = new Clock().jumpYears(GameInfo.YEAR_START).jumpDays(GameInfo.DAY_START).jumpHours(GameInfo.HOUR_START);
		appendLn(GameInfo.NAME + " V" + GameInfo.VERSION);
		appendLn(GameInfo.WHATSNEW);
		appendLn();

		CommandParser.initalize();

		Game.instance().window.requestFocus();
		Game.instance().requestFocusInWindow();

		start();

		String playerName = null;
		while (playerName == null || playerName.isEmpty()) {
			String temp = Game.instance().getTextInput("What is your name?").trim();
			if (temp != null && !temp.isEmpty()) {
				if (Game.instance().getChoiceInput("Is " + temp + " your name?", new String[] { "Yes", "No" }) == 0)
					playerName = temp;
			}
		}
		Game.instance().getPlayer().setName(playerName);
		Game.instance().append("Your name is now ");
		Game.instance().append(Game.instance().getPlayer().getName());
		Game.instance().append("! Nice job.");
		Game.instance().appendLn();
		Game.instance().appendLn();
		Game.instance().queueTurn();
		Game.instance().getPlayer().getCurrentArea().onEntry();
		Game.instance().playerHud.showNow(1500);
	}

	@Override
	public void run() {
		while (running) {
			timer++;
			if (timer > Long.MAX_VALUE)
				timer = 0;

			if (turnQueue > 0) {
				this.turn();
				turnQueue--;
			}

			playerHud.update();
			mapHud.update();
			this.draw();
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void turn() {
		clock.tick();
		if (player.getHealth() <= 0) {
			Game.instance().appendLn("=-=-GAME OVER-=-=");
			int choice = Game.instance().getChoiceInput("Restart?", new String[] { "Yes", "No" });
			if (choice == 0) {
				this.stop();
				new Game().start();
				return;
			}
		}

		for (Entity e : currentMap.getAllEntites()) {
			e.update();
		}
	}

	private void draw() {
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			this.createBufferStrategy(2);
			return;
		}

		Graphics2D g = (Graphics2D) bs.getDrawGraphics();
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, GameInfo.WIDTH, GameInfo.HEIGHT);
		g.setColor(Color.WHITE);
		g.setFont(Game.getGameFont());
		int lineAmt = getAmtLines();
		int sx = GameInfo.MARGIN[1];
		int sy = GameInfo.HEIGHT - CommandInputBar.BAR_HEIGHT - GameInfo.MARGIN[0] - TextRenderer.getFontHeight(g) * (lineAmt - screenOffset + 1);
		TextRenderer.drawString(g, screenText, sx, sy, GameInfo.TEXT_WIDTH);
		int sbarHeight = Math.max(10, (GameInfo.HEIGHT - CommandInputBar.BAR_HEIGHT) / lineAmt);
		int sbarY = (GameInfo.HEIGHT - CommandInputBar.BAR_HEIGHT - sbarHeight);
		if (screenOffset != 0)
			sbarY -= (int) ((GameInfo.HEIGHT - CommandInputBar.BAR_HEIGHT - sbarHeight) * ((double) screenOffset / (double) lineAmt));
		g.fillRect(GameInfo.WIDTH - 5, sbarY, 5, sbarHeight);
		inputBar.draw(g);

		playerHud.draw(g);
		mapHud.draw(g);

		g.dispose();
		bs.show();
	}

	public synchronized void start() {
		if (running)
			return;
		this.thread = new Thread(this);
		this.running = true;
		this.thread.start();
	}

	public synchronized void stop() {
		if (!running)
			return;
		this.running = false;
		try {
			this.thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new Game();
	}

	public static Game instance() {
		return INSTANCE;
	}

	public void queueTurn() {
		this.turnQueue++;
	}

	public void passTime(long turns) {
		this.turnQueue += turns;
	}

	public void append(String text) {
		this.screenText += text;
		if (autoScroll)
			this.screenOffset = 0;
		else
			this.screenOffset += getAmtLines(text);
	}

	public void appendLn(String text) {
		append(text + "\n");
	}

	public void appendLn() {
		append("\n");
	}

	public void clear() {
		this.screenText = "";
		this.screenOffset = 0;
	}

	public long getTimer() {
		return timer;
	}

	public static Font getGameFont(int style, float size) {
		return GAME_FONT.deriveFont(style, size);
	}

	public static Font getGameFont() {
		return GAME_FONT;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		inputBar.onKeyPressed(e);
		if (e.getKeyCode() == KeyEvent.VK_PAGE_UP)
			screenOffset += (GameInfo.HEIGHT - CommandInputBar.BAR_HEIGHT) / TextRenderer.getFontHeight((Graphics2D) this.getGraphics());
		if (e.getKeyCode() == KeyEvent.VK_PAGE_DOWN)
			screenOffset -= (GameInfo.HEIGHT - CommandInputBar.BAR_HEIGHT) / TextRenderer.getFontHeight((Graphics2D) this.getGraphics());
		if (e.getKeyCode() == KeyEvent.VK_HOME)
			screenOffset = 0;
		if (e.getKeyCode() == KeyEvent.VK_END)
			screenOffset = getAmtLines();

		if (screenOffset < 0)
			screenOffset = 0;
		if (screenOffset > getAmtLines()) {
			screenOffset = getAmtLines();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
		inputBar.onKeyTyped(e);
	}

	@Override
	public void focusGained(FocusEvent e) {
		hasFocus = true;
	}

	@Override
	public void focusLost(FocusEvent e) {
		hasFocus = false;
	}

	public void onCommand(String command) {
		appendLn(GameInfo.HANDLE + command + "\n");
		if (awaitingTextInput) {
			textInput = command;
			awaitingTextInput = false;
			return;
		}
		try {
			GameCommand<?> cmd = CommandParser.parseCommand(command);
			if (cmd != null) {
				cmd.doAction();
				passTime(cmd.getLength());
			}
		} catch (GameParseException e) {
			appendLn(e.getMessage());
			appendLn();
		}
	}

	public boolean hasFocus() {
		return hasFocus;
	}

	public PC getPlayer() {
		return player;
	}

	/**
	 * USE OUTSIDE OF MAIN GAME THREAD!
	 * 
	 * @param prompt
	 * @return
	 */
	public String getTextInput(String prompt) {
		if (awaitingTextInput)
			return null;
		appendLn(prompt);
		appendLn();
		awaitingTextInput = true;
		while (awaitingTextInput) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		String s = textInput;
		textInput = "";
		return s;
	}

	/**
	 * USE OUTSIDE OF MAIN GAME THREAD
	 * 
	 * @param prompt
	 * @param choices
	 * @return
	 */
	public int getChoiceInput(String prompt, String[] choices) {
		if (awaitingTextInput)
			return -1;
		StringBuilder pmpt = new StringBuilder();
		pmpt.append(prompt);
		pmpt.append("\n\n");
		for (int i = 0; i < choices.length; i++) {
			pmpt.append((i + 1) + ") ");
			pmpt.append(choices[i]);
			pmpt.append("\n");
		}
		String s = getTextInput(pmpt.toString());
		try {
			int choice = Integer.parseInt(s);
			if (choice < 1 || choice > choices.length) {
				appendLn(s + " is not a valid option.\n");
				return getChoiceInput(pmpt.toString(), choices);
			}
			return choice - 1;
		} catch (NumberFormatException e) {
			appendLn(s + " is not a valid option.\n");
			return getChoiceInput(pmpt.toString(), choices);
		}
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if (e.getWheelRotation() < 0)
			screenOffset++;
		if (e.getWheelRotation() > 0)
			screenOffset--;
		if (screenOffset < 0)
			screenOffset = 0;
		if (screenOffset > getAmtLines()) {
			screenOffset = getAmtLines();
		}
	}

	public int getAmtLines() {
		return getAmtLines(this.screenText);
	}

	public int getAmtLines(String s) {
		BufferStrategy bs = getBufferStrategy();
		if (bs == null)
			return -1;
		Graphics2D g = (Graphics2D) bs.getDrawGraphics();
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g.setFont(Game.getGameFont());
		return TextRenderer.getStringLineAmt(g, s, GameInfo.WIDTH);
	}

	public Map getCurrentMap() {
		return currentMap;
	}

	public void onPlayerStatChange() {
		this.playerHud.show(2000);
	}

	public boolean toggleAutoScroll() {
		return autoScroll = !autoScroll;
	}

	public boolean isAwaitingTextInput() {
		return awaitingTextInput;
	}

	public SSDatabase serialize() {
		SSDatabase db = new SSDatabase("game");

		SSObject gameData = new SSObject("gameData");
		gameData.addObject(clock.serialize());
		gameData.addField(SSField.Boolean("setngAutoScrl", autoScroll));

		db.addObject(gameData);

		db.addObject(currentMap.serialize());
		db.addObject(player.serialize());

		return db;
	}

	public void deserialize(SSDatabase data) {
		SSObject gameData = data.getObject("gameData");

		autoScroll = gameData.getField("setngAutoScrl").getBoolean();
		clock.deserialize(gameData.getObject(clock.getSerialName()));
		currentMap = new Map();
		currentMap.deserialize(data.getObject("map"));
		player.deserialize(data.getObject("pc"));
	}

	public void saveGame(String profName) {
		SSDatabase db = serialize();
		SSObject fileData = new SSObject("fileData");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy:MM:dd;HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		fileData.addString(new SSString("gameInfoString", dtf.format(now) + " : " + player.getName() + ", Level " + player.getLevel() + ", " + clock.toShortString()));
		db.addObject(fileData);

		byte[] data = new byte[db.getSize()];
		db.writeBytes(data, 0);
		Game.instance().appendLn("Saving game to " + profName + "... (" + (data.length / 1000.0) + " KB)");
		String filePath = GameInfo.SAVE_FOLDER_PATH + File.separator + profName + SSDatabase.FILE_EXTENTION;
		File f = new File(filePath);
		f.getParentFile().mkdirs();
		try {
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(filePath));
			stream.write(data);
			stream.close();
			System.out.println("Wrote " + profName + SSDatabase.FILE_EXTENTION + " successfully to: " + filePath + " (" + data.length + " bytes)");
		} catch (Exception e) {
			System.err.println("Can't write " + profName + SSDatabase.FILE_EXTENTION + " file to: " + filePath);
			Game.instance().appendLn("Error.");
			Game.instance().appendLn();
			return;
		}
		Game.instance().appendLn("Done.");
		Game.instance().appendLn();
	}

	public void loadGame(String profName) {
		String filePath = GameInfo.SAVE_FOLDER_PATH + File.separator + profName + SSDatabase.FILE_EXTENTION;
		File file = new File(filePath);
		SSDatabase orig = serialize();

		try {
			Game.instance().appendLn("Loading game from " + profName + "... (" + (Files.readAllBytes(file.toPath()).length / 1000.0) + " KB)");

			byte[] data = Files.readAllBytes(file.toPath());
			SSDatabase db = SSDatabase.Deserialize(data);
			deserialize(db);
		} catch (Exception e) {
			System.err.println("Can't read " + profName + SSDatabase.FILE_EXTENTION + " file from: " + filePath + ".");
			Game.instance().appendLn("Error: Can't load " + profName);
			deserialize(orig);
			return;
		}
		Game.instance().appendLn("Done.");
		Game.instance().appendLn();
		Game.instance().getPlayer().getCurrentArea().onEntry();
	}

	public File[] getSaveFiles() {
		File saveDir = new File(GameInfo.SAVE_FOLDER_PATH);
		if (!saveDir.exists()) {
			saveDir.mkdirs();
		}
		File[] files = saveDir.listFiles();
		List<File> validFiles = new ArrayList<File>();
		try {
			for (int i = 0; i < files.length; i++) {
				String fileName = files[i].getName();
				if (fileName.substring(fileName.indexOf('.')) == SSDatabase.FILE_EXTENTION) {
					try {
						SSDatabase db = SSDatabase.Deserialize(Files.readAllBytes(files[i].toPath()));
						if (db != null)
							validFiles.add(files[i]);
					} catch (SSException e) {
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return files;
	}

	/**
	 * USE OUTSIDE OF MAIN GAME THREAD
	 */
	public void showLoadMenu() {
		File[] saveFiles = getSaveFiles();
		if (saveFiles.length == 0) {
			Game.instance().appendLn("No saved files.");
			return;
		}
		String[] fileNames = new String[saveFiles.length + 1];
		try {
			for (int i = 0; i < saveFiles.length; i++) {
				String gameInfoString = "[LOAD ERROR]";
				try {
					SSObject fileData = SSDatabase.Deserialize(Files.readAllBytes(saveFiles[i].toPath())).getObject("fileData");
					gameInfoString = fileData.getString("gameInfoString").getString();
				} catch (SSException e) {
				}
				fileNames[i] = saveFiles[i].getName().substring(0, saveFiles[i].getName().indexOf('.')) + " : " + gameInfoString;

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		fileNames[saveFiles.length] = "Cancel";
		int choice = Game.instance().getChoiceInput("Select save file to load:", fileNames);
		if (choice == saveFiles.length) {
			Game.instance().appendLn("Cancelled.");
			return;
		}
		loadGame(saveFiles[choice].getName().substring(0, saveFiles[choice].getName().indexOf('.')));
	}

	public void deleteSave(String profName) {
		String filePath = GameInfo.SAVE_FOLDER_PATH + File.separator + profName + SSDatabase.FILE_EXTENTION;
		File file = new File(filePath);
		boolean deleted = false;
		if (file.exists())
			deleted = file.delete();

		if (deleted) {
			Game.instance().appendLn("Deleted file: " + profName);
		} else {
			Game.instance().appendLn("Can't delete file: " + profName);
		}
	}

	/**
	 * USE OUTSIDE OF MAIN GAME THREAD
	 */
	public void showDeleteMenu() {
		File[] saveFiles = getSaveFiles();
		if (saveFiles.length == 0) {
			Game.instance().appendLn("No saved files.");
			return;
		}
		String[] fileNames = new String[saveFiles.length + 1];
		for (int i = 0; i < saveFiles.length; i++) {
			try {
				String gameInfoString = "[LOAD ERROR]";
				try {
					SSObject fileData = SSDatabase.Deserialize(Files.readAllBytes(saveFiles[i].toPath())).getObject("fileData");
					gameInfoString = fileData.getString("gameInfoString").getString();
				} catch (SSException e1) {
				}
				fileNames[i] = saveFiles[i].getName().substring(0, saveFiles[i].getName().indexOf('.')) + " : " + gameInfoString;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		fileNames[saveFiles.length] = "Cancel";
		int choice = Game.instance().getChoiceInput("Select save file to delete:", fileNames);
		if (choice == saveFiles.length) {
			Game.instance().appendLn("Cancelled.");
			return;
		}
		deleteSave(saveFiles[choice].getName().substring(0, saveFiles[choice].getName().indexOf('.')));
	}

	public Clock getClock() {
		return clock;
	}

	public long getTurn() {
		return clock.getTotalTimeSeconds();
	}

}
