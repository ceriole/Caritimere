package cereal.cmere.map;

import java.util.HashMap;
import java.util.HashSet;

import com.google.common.collect.ImmutableList;

public enum Direction {

	NORTH(ImmutableList.of("north", "n"), 0, 1, 0), SOUTH(ImmutableList.of("south", "s"), 0, -1, 0),

	EAST(ImmutableList.of("east", "e"), 1, 0, 0), WEST(ImmutableList.of("west", "w"), -1, 0, 0),

	UP(ImmutableList.of("up", "u"), 0, 0, 1), DOWN(ImmutableList.of("down", "d"), 0, 0, -1),

	NORTHEAST(ImmutableList.of("northeast", "ne"), 1, 1, 0), NORTHWEST(ImmutableList.of("northwest", "nw"), 1, -1, 0),

	SOUTHEAST(ImmutableList.of("southeast", "se"), 1, -1, 0), SOUTHWEST(ImmutableList.of("southwest", "sw"), -1, -1, 0);

	private static final HashMap<Direction, Direction> OPPOSITES = new HashMap<>();
	public static final HashSet<Direction> CARDINALS = new HashSet<>();
	static {
		CARDINALS.add(NORTH);
		CARDINALS.add(SOUTH);
		CARDINALS.add(EAST);
		CARDINALS.add(WEST);
		CARDINALS.add(UP);
		CARDINALS.add(DOWN);

		OPPOSITES.put(NORTH, SOUTH);
		OPPOSITES.put(SOUTH, NORTH);

		OPPOSITES.put(EAST, WEST);
		OPPOSITES.put(WEST, EAST);

		OPPOSITES.put(NORTHEAST, SOUTHWEST);
		OPPOSITES.put(SOUTHWEST, NORTHEAST);

		OPPOSITES.put(SOUTHEAST, NORTHWEST);
		OPPOSITES.put(NORTHWEST, SOUTHEAST);

		OPPOSITES.put(DOWN, UP);
		OPPOSITES.put(UP, DOWN);
	}

	private final ImmutableList<String> aliases;
	private final int dx, dy, dz;

	private Direction(ImmutableList<String> aliases, int dx, int dy, int dz) {
		this.aliases = aliases;
		this.dx = dx;
		this.dy = dy;
		this.dz = dz;
	}

	public MapPos from(ILocatable loc) {
		return loc.getPos().add(dx, dy, dz);
	}

	public Direction opposite() {
		return OPPOSITES.get(this);
	}

	public ImmutableList<String> getAliases() {
		return aliases;
	}

	public String getName() {
		return aliases.get(0);
	}
}
