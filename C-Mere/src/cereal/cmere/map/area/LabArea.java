package cereal.cmere.map.area;

import cereal.cmere.game.command.GameCommand;
import net.keabotstudios.superserial.containers.SSField;
import net.keabotstudios.superserial.containers.SSObject;

public class LabArea extends Area {

	private boolean seen = false;

	public LabArea() {
		this(0, 0);
	}

	public LabArea(int miniMapX, int miniMapY) {
		super("Abandoned Lab", miniMapX, miniMapY);
	}

	@Override
	public void onEntry() {
		super.onEntry();
	}

	@Override
	public void onExit() {
		seen = true;
	}

	@Override
	public String getAreaText() {
		if (!seen)
			return "You are on the floor, surrounded in the green liquid that held you for your entire life. " + "You try and look around the room you are in, but it is too dark for you to see. " + "You are then startled by a beep, and the room illuminates with a dim yellow light. " + "There are creatures all around you, laying on the floor. " + "Some are covered in a red liquid that smells metallic. Lab equipment is broken and scattered along the floor. " + "You try pulling yourself up onto your feet. After some struggling, you finally manage to do so, albeit a bit shaky." + "\nYou wonder what to do next.";
		else
			return "The lab is still decrepit, the green liquid you were suspended squelches under your feet.\n\nKinky.";
	}

	@Override
	public void onInteracted(GameCommand<? extends GameCommand<?>> cmd) {

	}

	@Override
	protected void serializeArea(SSObject obj) {
		obj.addField(SSField.Boolean("seen", seen));
	}

	@Override
	protected void deserializeArea(SSObject data) {
		this.seen = data.getField("seen").getBoolean();
	}

}
