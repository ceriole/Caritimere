package cereal.cmere;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import com.google.common.collect.ImmutableList;

public class Utils {

	public static final String VOWELS = "aeiou";

	/**
	 * Finds the article of a word.
	 * 
	 * @param s
	 *            The word.
	 * @return "a" or "an".
	 */
	public static String article(String s) {
		if (VOWELS.contains("" + s.toLowerCase().indexOf(0))) {
			return "an";
		} else
			return "a";
	}

	/**
	 * This finds the max number in a list.
	 * 
	 * @param list
	 *            A list of numbers.
	 * @return The largest one.
	 */
	public static int getMaxNumber(int[] list) {
		int max = list[0];
		for (int i = 1; i < list.length; i++)
			if (list[i] > max)
				max = list[i];
		return max;
	}

	/**
	 * This finds the max number in a list.
	 * 
	 * @param list
	 *            A list of numbers.
	 * @return The largest one.
	 */
	public static int getMaxNumber(Integer[] list) {
		int max = list[0];
		for (int i = 1; i < list.length; i++)
			if (list[i] > max)
				max = list[i];
		return max;
	}

	/**
	 * Turns an {@link ImmutableList} of strings lower case.
	 * 
	 * @param imSet
	 *            An {@link ImmutableList} that contains strings.
	 * @return An {@link ImmutableList} with strings that are lower case.
	 * @see {@link String#toLowerCase()}
	 */
	public static ImmutableList<String> immutableStringListToLowerCase(ImmutableList<String> list) {
		List<String> lowerCaseList = new ArrayList<>();
		for (String s : list) {
			lowerCaseList.add(s.toLowerCase());
		}
		return ImmutableList.copyOf(lowerCaseList);
	}

	public static String makeDelimedList(String[] strings, String delim) {
		StringBuilder out = new StringBuilder();
		for (int i = 0; i < strings.length; i++) {
			out.append(strings[i]);
			if (i < strings.length - 1)
				out.append(delim);
		}
		return out.toString();
	}

	/**
	 * Calculate the Levenshtein Distance between two strings (the number of
	 * insertions, deletions, and substitutions needed to transform the first string
	 * into the second).
	 * 
	 * @param source
	 *            The source string.
	 * @param target
	 *            The target string.
	 * @return The Levenstein distance.
	 */
	public static long levenstheinDistance(String source, String target) {
		return levenstheinDistance(source, source.length(), target, target.length());
	}

	/**
	 * Calculate the Levenshtein Distance between two strings (the number of
	 * insertions, deletions, and substitutions needed to transform the first string
	 * into the second).
	 * 
	 * @param source
	 *            The source string.
	 * @param srcLen
	 *            The length of the source string.
	 * @param target
	 *            The target string.
	 * @param targLen
	 *            The length of the target string.
	 * @return The Levenstein distance.
	 */
	public static long levenstheinDistance(String source, int srcLen, String target, int targLen) {
		if (srcLen > source.length())
			throw new IllegalArgumentException("Source length given is greater than the actual source length!");
		if (targLen > target.length())
			throw new IllegalArgumentException("Target length given is greater than the actual target length!");

		long cost;

		if (srcLen == 0)
			return targLen;
		if (targLen == 0)
			return srcLen;

		if (source.charAt(srcLen - 1) == target.charAt(targLen - 1))
			cost = 0;
		else
			cost = 1;

		return min(levenstheinDistance(source, srcLen - 1, target, targLen) + 1, levenstheinDistance(source, srcLen, target, targLen - 1) + 1, levenstheinDistance(source, srcLen - 1, target, targLen - 1) + cost);
	}

	public static long min(long... nums) {
		if (nums.length < 1)
			throw new IllegalArgumentException("Give at least one number!");
		if (nums.length < 2)
			return nums[0];
		long min = nums[0];
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] < min)
				min = nums[i];
		}
		return min;
	}

	public static long[] getNLowest(long[] array, int n) {
		if (n > array.length)
			throw new IllegalArgumentException("N must be less than or equal to the array length!");
		Arrays.sort(array);
		return Arrays.copyOf(array, n);
	}

	/**
	 * Gets the lowest mapped value.
	 * 
	 * @param map
	 *            Map with the key type of long.
	 * @return The value with the lowest key.
	 */
	public static <V> V getLowestMappedValue(Map<Long, V> map) {
		Long[] arr = map.keySet().toArray(new Long[map.size()]);
		long min = Long.MAX_VALUE;
		for (int i = 0; i < arr.length; i++) {
			long l = arr[i].longValue();
			if (l < min)
				min = l;
		}
		return map.get(min);
	}

	public static BufferedImage loadImage(String path) {
		try {
			return ImageIO.read(Game.class.getResourceAsStream(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}