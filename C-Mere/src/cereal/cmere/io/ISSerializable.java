package cereal.cmere.io;

import net.keabotstudios.superserial.containers.SSObject;

public interface ISSerializable {

	public SSObject serialize();

	public void deserialize(SSObject data);

	public String getSerialName();

}
