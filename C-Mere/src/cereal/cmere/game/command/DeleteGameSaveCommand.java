package cereal.cmere.game.command;

import cereal.cmere.Game;

public class DeleteGameSaveCommand extends GameCommand<DeleteGameSaveCommand> {

	public String profileName;

	public DeleteGameSaveCommand(CommandTemplate<DeleteGameSaveCommand> parent) {
		super(parent, 0, (DeleteGameSaveCommand cmd) -> {
			new Thread(new Runnable() {
				@Override
				public void run() {
					if (cmd.profileName == null)
						Game.instance().showDeleteMenu();
					else
						Game.instance().deleteSave(cmd.profileName);

					try {
						Thread.currentThread().join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}).start();
		});
	}

}
