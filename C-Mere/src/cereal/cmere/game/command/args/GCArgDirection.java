package cereal.cmere.game.command.args;

import cereal.cmere.map.Direction;

public class GCArgDirection extends GameCommandArg<Direction> {

	public GCArgDirection(boolean required) {
		super(Type.DIRECTION, "direction", required);
	}

	@Override
	public Direction parse(String str) throws CommandArgParseException {
		for (Direction d : Direction.values()) {
			if (d.getAliases().contains(str.toLowerCase())) {
				return d;
			}
		}
		throw new CommandArgParseException(this, "Not a valid direction: " + str);
	}

}
