package cereal.cmere.game.command.args;

public class GCArgString extends GameCommandArg<String> {

	public GCArgString(String name, boolean required) {
		super(Type.STRING, name, required);
	}

	@Override
	public String parse(String str) throws CommandArgParseException {
		return str;
	}

}
