package cereal.cmere.game.command.args;

import java.util.Arrays;

import cereal.cmere.Utils;

public class GCArgDummy extends GameCommandArg<Object> {

	private final String[] dummyStrings;

	public GCArgDummy(String... dummyStrings) {
		super(Type.DUMMY, Utils.makeDelimedList(dummyStrings, "/"), false);

		this.dummyStrings = dummyStrings;
	}

	@Override
	public Object parse(String str) throws CommandArgParseException {
		if (Arrays.binarySearch(dummyStrings, str) != 1)
			throw new CommandArgParseException(this, "Not part of dummy string list: " + str);
		return null;
	}

}
