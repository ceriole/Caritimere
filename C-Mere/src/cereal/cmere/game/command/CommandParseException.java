package cereal.cmere.game.command;

public class CommandParseException extends GameParseException {
	private static final long serialVersionUID = 1L;

	private final GameCommand<?> cmd;

	public CommandParseException(GameCommand<?> cmd, String error) {
		super(cmd + ": " + error);
		this.cmd = cmd;
	}

	@Override
	public String getMessage() {
		return this.msg;
	}

	public GameCommand<?> getCommand() {
		return cmd;
	}

}
