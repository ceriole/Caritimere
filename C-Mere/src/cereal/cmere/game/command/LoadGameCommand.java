package cereal.cmere.game.command;

import cereal.cmere.Game;

public class LoadGameCommand extends GameCommand<LoadGameCommand> {

	public String profileName;

	public LoadGameCommand(CommandTemplate<LoadGameCommand> parent) {
		super(parent, 0, (LoadGameCommand cmd) -> {
			new Thread(new Runnable() {
				@Override
				public void run() {
					if (cmd.profileName == null)
						Game.instance().showLoadMenu();
					else
						Game.instance().loadGame(cmd.profileName);

					try {
						Thread.currentThread().join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}).start();
		});
	}

}
