package cereal.cmere.game.command;

@FunctionalInterface
public interface IGameAction<T extends GameCommand<T>> {
	public void doAction(T cmd);
}
