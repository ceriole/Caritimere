package cereal.cmere.game.command;

import com.google.common.collect.ImmutableList;

import cereal.cmere.Game;
import cereal.cmere.game.command.args.GCArgDummy;
import cereal.cmere.game.command.args.GCArgTargetable;

public class LookCommandTemplate extends CommandTemplate<LookCommand> {

	public LookCommandTemplate() {
		super("look", "Look at something.", ImmutableList.of("look", "l"), ImmutableList.of(new GCArgDummy("at"), new GCArgTargetable(false)));
	}

	@Override
	public LookCommand parseCommand(String str) throws GameParseException {
		LookCommand cmd = new LookCommand(this);
		String[] args = CommandParser.parseArgs(str);

		if (args.length == 0)
			cmd.target = Game.instance().getPlayer().getCurrentArea();
		else if (args.length < 3)
			if (args.length == 2) {
				cmd.target = ((GCArgTargetable) this.args.get(1)).parse(args[1]);
			} else {
				cmd.target = ((GCArgTargetable) this.args.get(1)).parse(args[0]);
			}
		return cmd;
	}

}
