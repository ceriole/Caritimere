package cereal.cmere.game.effect;

import cereal.cmere.Game;
import cereal.cmere.entity.Entity;
import cereal.cmere.game.IDescribeable;

public abstract class StatusEffect implements IDescribeable {

	private long startTime, effectLength, effectInterval = 0;

	private boolean active;

	private String name, description;

	public StatusEffect(String name, String description, long effectLength) {
		this.name = name;
		this.description = description;
		this.effectLength = effectLength;
	}

	public boolean shouldDoEffect() {
		return getEffectLengthRemaining() % effectInterval == 0;
	}

	public void startEffect() {
		startTime = Game.instance().getTurn();
		active = true;
	}

	public void endEffect() {
		startTime = 0;
		active = false;
	}

	public long getEffectLengthRemaining() {
		if (!active)
			return -1;
		return Game.instance().getTurn() - startTime;
	}

	public boolean shouldRemove() {
		return !active && Game.instance().getTurn() - startTime > effectLength;
	}

	abstract void doEffect(Entity e);

	abstract void undoEffect(Entity e);

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getName() {
		return name;
	}

}
