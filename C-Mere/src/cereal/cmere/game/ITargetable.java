package cereal.cmere.game;

import com.google.common.collect.ImmutableList;

public interface ITargetable {

	public ImmutableList<String> getAliases();

	public boolean canTarget();

}
