package cereal.cmere.game;

import cereal.cmere.map.Direction;
import cereal.cmere.map.ILocatable;

public interface IMoveable extends ILocatable {

	public default boolean move(Direction d) {
		return setPos(d.from(getPos()));
	}

	/**
	 * @return The amount of time (in game seconds/updates) it takes for this
	 *         {@link IMoveable} to move.
	 */
	public long getGetMoveLength();

}
