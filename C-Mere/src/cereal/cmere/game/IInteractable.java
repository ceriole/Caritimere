package cereal.cmere.game;

import cereal.cmere.game.command.GameCommand;

public interface IInteractable extends ITargetable {

	public boolean isHidden();

	public boolean isInteractable();

	public void onInteracted(GameCommand<? extends GameCommand<?>> cmd);

	@Override
	default boolean canTarget() {
		return !isHidden() && isInteractable();
	}

}
